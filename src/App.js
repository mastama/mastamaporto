import React from "react";
import "bootstrap/dist/css/bootstrap.min.css";

import { About, Footer, Header} from "./container";
import { Navbar } from "./components";
import './App.scss';
import Projects from "./container/Projects/Projects";

const App = () => {
    return ( 
        <div className="app">
            <Navbar/>
            <Header/>
            <About />
            <Projects />
            <Footer />
        </div>
    );
}

export default App;