import React from "react";
import { Container, Row, Col } from "react-bootstrap";

import { BsLinkedin, BsInstagram, BsGithub } from "react-icons/bs";
import { FaGitlab, FaCodepen, FaMediumM } from 'react-icons/fa';

import "./Footer.css";

function Footer() {
  let date = new Date();
  let year = date.getFullYear();
  return (
    <Container fluid className="footer">
      <Row>
        <Col md="4" className="footer-copywright">
          <h3>Developed by Singgih Pratama || Mastama </h3>
        </Col>
        <Col md="4" className="footer-copywright">
          <h3>Copyright © {year} tanbyte</h3>
        </Col>
        <Col md="4" className="footer-body">
          <ul className="footer-icons">
            <li className="social-icons">
              <a
                href="https://www.linkedin.com/in/singgih-pratama/"
                style={{ color: "white" }}
                target="_blank"
                rel="noopener noreferrer"
              >
                <BsLinkedin />
              </a>
            </li>
            <li className="social-icons">
            <a
                href="https://www.instagram.com/s_mastama/"
                style={{ color: "white" }}
                target="_blank" 
                rel="noopener noreferrer"
              >
                <BsInstagram />
              </a>
            </li>
            <li className="social-icons">
            <a
                href="https://github.com/mastama"
                style={{ color: "white" }}
                target="_blank" 
                rel="noopener noreferrer"
              >
                <BsGithub />
              </a>
            </li>
            <li className="social-icons">
            <a
                href="https://gitlab.com/mastama"
                style={{ color: "white" }}
                target="_blank" 
                rel="noopener noreferrer"
              >
                <FaGitlab />
              </a>
            </li>
            <li className="social-icons">
            <a
                href="https://codepen.io/mastama"
                style={{ color: "white" }}
                target="_blank" 
                rel="noopener noreferrer"
              >
                <FaCodepen />
              </a>
            </li>
            <li className="social-icons">
            <a
                href="https://medium.com/@mastamau"
                style={{ color: "white" }}
                target="_blank" 
                rel="noopener noreferrer"
              >
                <FaMediumM />
              </a>
            </li>
          </ul>
        </Col>
      </Row>
    </Container>
  );
}

export default Footer;
