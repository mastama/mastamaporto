import React from "react";
import Card from "react-bootstrap/Card";
import { ImPointRight } from "react-icons/im";

function AboutCard() {
  return (
    <Card className="quote-card-view">
      <Card.Body>
        <blockquote className="blockquote mb-0">
          <p style={{ textAlign: "justify" }}>
            Hi Everyone, I am <span className="purple">Singgih Pratama, </span>
            from <span className="purple"> Depok, Indonesia.</span>
            <br />I am an undergraduate student in Math Education.
            <br />
            <br />
            Apart from coding, some other activities that I love to do!
          </p>
          <ul>
            <li className="about-activity">
              <ImPointRight /> Praying
            </li>
            <li className="about-activity">
              <ImPointRight /> Studying
            </li>
            <li className="about-activity">
              <ImPointRight /> Travelling
            </li>
          </ul>

          <p style={{ color: "RGBA( 0, 191, 255, 1 )" }}>
            "Strive to build things that make a difference!"{" "}
          </p>
          <footer className="blockquote-footer">Mastama</footer>
        </blockquote>
      </Card.Body>
    </Card>
  );
}

export default AboutCard;