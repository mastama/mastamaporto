import React from "react";
import { BsLinkedin, BsInstagram, BsGithub } from "react-icons/bs";
import { FaGitlab, FaCodepen, FaMediumM } from 'react-icons/fa';

const SocialMedia = () => {
  return (
      <div className="app__social">
        <div>
          <a href="https://www.linkedin.com/in/singgih-pratama/">
            <BsLinkedin />
          </a>
        </div>
        <div>
          <a href="https://www.instagram.com/s_mastama/">
            <BsInstagram />
          </a>
        </div>
        <div>
          <a href="https://github.com/mastama">
            <BsGithub />
          </a>
        </div>
        <div>
          <a href="https://gitlab.com/mastama">
            <FaGitlab />
          </a>
        </div>
        <div>
          <a href="https://codepen.io/mastama">
            <FaCodepen />
          </a>
        </div>
        <div>
          <a href="https://medium.com/@mastamau">
            <FaMediumM />
          </a>
        </div>
      </div>
  );
};

export default SocialMedia;
